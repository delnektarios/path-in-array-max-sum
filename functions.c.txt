#include <stdio.h>
#include <stdlib.h>

int max(int a,int b,int c){     /*function that returns the maximum of 3 values*/
	int max=a;
	if (a<b && c<b) max=b;
	if (a<c && b<c) max=c;
	if (c<a && b<a) max=a;
	return max;
}

int pureRecursive( int i,int j,int lines,int columns,int** arr){
	int sum,sum1,sum2,sum3;      /*vars that hold the 3 possible sums*/
	i++;                         /*in order the function begins from the first line ,the first value of i is -1(from main)*/
	if (i==(lines-1)) sum=arr[i][j];   /*in case the function reached the last line, ther is nothing to sum*/           
	if (i<(lines-1) && j==0){          /*each of the following cases directs the function to calculate the sums correctly*/
		sum1=arr[i][j]+pureRecursive(i,columns-1,lines,columns,arr);/*function calls itself and goes deeper in table*/
		sum2=arr[i][j]+pureRecursive(i,j,lines,columns,arr);
		sum3=arr[i][j]+pureRecursive(i,j+1,lines,columns,arr);
		sum=max(sum1,sum2,sum3);}                                   /*f returns the maximum sum*/
	if (i<(lines-1) && (j>0 && j<(columns-1))){
		sum1=arr[i][j]+pureRecursive(i,j-1,lines,columns,arr);
		sum2=arr[i][j]+pureRecursive(i,j,lines,columns,arr);
		sum3=arr[i][j]+pureRecursive(i,j+1,lines,columns,arr);
		sum=max(sum1,sum2,sum3);}
	if (i<(lines-1) && j==(columns-1)){
		sum1=arr[i][j]+pureRecursive(i,j-1,lines,columns,arr);
		sum2=arr[i][j]+pureRecursive(i,columns-1,lines,columns,arr);
		sum3=arr[i][j]+pureRecursive(i,0,lines,columns,arr);
		sum=max(sum1,sum2,sum3);}
	return sum;
}
    
int matrRecursive (int i,int j,int lines,int columns,int **arr,int **p){/*this function works as the above(with one difference!)*/
	int sum,sum1,sum2,sum3;        /*table p is used to hold any sum that is calculated once,in order not re-calculating the same twice*/
	i++;
	if (p[i][j]!=-1) return p[i][j];/*any cell of table p is not equal to -1,only in case it holds a sum*/
	if (p[i][j]==-1){               /*otherwise,the function calculates the missing sum*/
		if (i==(lines-1)) sum=p[i][j];   /*the same as the above function*/
		if (i<(lines-1) && j==0){
			sum1=arr[i][j]+matrRecursive(i,columns-1,lines,columns,arr,p);
			sum2=arr[i][j]+matrRecursive(i,j,lines,columns,arr,p);
			sum3=arr[i][j]+matrRecursive(i,j+1,lines,columns,arr,p);
			sum=max(sum1,sum2,sum3);
			if (sum==sum1) p[i][j]=sum; /*the difference(see first comment) is that the maximum sum found is kept in table p*/
			if (sum==sum2) p[i][j]=sum;
			if (sum==sum3) p[i][j]=sum;}
		if (i<(lines-1) && (j>0 && j<(columns-1))){
			sum1=arr[i][j]+matrRecursive(i,j-1,lines,columns,arr,p);
			sum2=arr[i][j]+matrRecursive(i,j,lines,columns,arr,p);
			sum3=arr[i][j]+matrRecursive(i,j+1,lines,columns,arr,p);
			sum=max(sum1,sum2,sum3);
			if (sum==sum1) p[i][j]=sum;
			if (sum==sum2) p[i][j]=sum;
			if (sum==sum3) p[i][j]=sum;}
		if (i<(lines-1) && j==(columns-1)){
			sum1=arr[i][j]+matrRecursive(i,j-1,lines,columns,arr,p);
			sum2=arr[i][j]+matrRecursive(i,columns-1,lines,columns,arr,p);
			sum3=arr[i][j]+matrRecursive(i,0,lines,columns,arr,p);
			sum=max(sum1,sum2,sum3);
			if (sum==sum1) p[i][j]=sum;
			if (sum==sum2) p[i][j]=sum;
			if (sum==sum3) p[i][j]=sum;}
		return p[i][j];  /*function returns the value in cell p[i][j],which is the last sum that was calculated*/
	}
}




void interative (int **arr,int lines,int columns){
int i,j,k=0,**p,s1,s2,s3,m=0,flag=1;
	p=malloc(lines*sizeof(int*));       /*creation of the table needed*/
	if (p!=NULL){
	for (i=0; i<=(lines-1); i++){
		p[i]=malloc(columns*sizeof(int));
		if (p[i]==NULL){
			printf("memory error!");
			flag=0;}
	}
	}
	if (!p){
    printf("memory error!");
    flag=0;}
	if (flag){             /*in case the creation was succesful,the function procceds to calculate the sums*/
		for (i=0; i<=(lines-1); i++){
			for (j=0; j<=(columns-1); j++) p[i][j]=arr[i][j];
			}
   for (i=(lines-2); i>=0; i--){
		s1=s2=s3=0;
		for (j=0; j<=(columns-1); j++){
			if (j==0)s1=p[i][j]+p[i+1][columns-1];
			if (j>0) s1=p[i][j]+p[i+1][j-1];
			s2=p[i][j]+p[i+1][j];
			if (j==(columns-1)) s3=p[i][j]+p[i+1][0];
			if (j<(columns-1))  s3=p[i][j]+p[i+1][j+1];
			p[i][j]=max(s1,s2,s3);}   /*every "if-case",of the above,directs the function*/
	}
	for (i=0; i<=(lines-1); i++){
	m=0;
	if(i==0){
	   for (j=0; j<=(columns-1); j++){
			if (m<p[i][j]){ m=p[i][j]; k=j;}
	}
	printf("\nMax sum is: %d\n%d",m,arr[i][k]);}          /*printing the maximum sum and the path*/
	if (i){
			if (k==0){
			    m=max(p[i][columns-1],p[i][k],p[i][k+1]); /*function max returns the maximum sum calculated*/
			    if (m==p[i][columns-1]) k=columns-1;      /*var k is used to direct the right path*/
			    if (m==p[i][k+1]) k+=1;
			    printf("->%d",arr[i][k]);
			    continue;}
			if (k==(columns-1)){
			    m=max(p[i][k-1],p[i][k],p[i][0]);
			    if (m==p[i][k-1]) k-=1;
			    if (m==p[i][0]) k=0;
			    printf("->%d",arr[i][k]);
				continue;}
			if (k>0 && k<(columns-1)){
			    m=max(p[i][k-1],p[i][k],p[i][k+1]);
			    if (m==p[i][k-1]) k-=1;
			    if (m==p[i][k+1]) k+=1;
			    printf("->%d",arr[i][k]);
			    continue;}
				}
			}printf("\n");
		}
    for (i=0; i<=(lines-1); i++){    /*function free frees any memory used by table p*/
        free(p[i]);
    }
    free(p);
}