Calculate the path in an integer array starting from one element of the top and stepping in down left,down right or down cell 
creating also a path that led to this maximum sum.

pureRecursive function runs the algorithm using recursion and no additional array (therefore it does not calculate the path, ony the max sum)

matrRecursive function runs the algorithm using recursion and an additional array

iterative function runs the algorithm using iteration and an additional array

Give text.txt as input ./executable < text.txt for example.

execution example :

>gcc -o tes *.c
>./test
4 4
1 2 3 4
4 3 2 1
1 3 4 2
2 4 3 1
Running pureRecursive
Max sum is: 15
Running matrRecursive
Max sum is: 15
4->4->3->4               (this is the path!!!!)
Running iterative 
Max sum is: 15
4->4->3->4 
>