#include <stdio.h>
#include <stdlib.h>
#include "functions.h"
#define MATRREC
#define ITER
#define PUREREC

void solve(int,int,int**);

void Purerec(int,int,int**);

void Matrrec(int,int,int**);

void Iter(int,int,int**);


int main(void){
    int lines,columns,**arr,flag=1,i,j,size=0;
	scanf("%d%d",&lines,&columns);               /*user enters dimensions of table*/
	arr=malloc(lines*sizeof(int*));              /*creation of desirable table*/                             
	if (arr!=NULL){                              /*succesful memory allocation*/
		for (i=0; i<=(lines-1); i++){            
			arr[i]=malloc(columns*sizeof(int));  /*creation of the second dimension of table*/
			if (arr[i]==NULL){                   /*failed memory allocation*/
			    printf("memory error!");         
			    flag=0;}
		}
	}
	if (arr==NULL){                              /*failed memory allocation*/
	    printf("memory error!");
		flag=0;
	}
	if (flag){                                   /*in case successful memory allocation*/
		for (i=0; i<=(lines-1); i++){
			for (j=0; j<=(columns-1); j++){
				scanf("%d",&arr[i][j]);          /*user fills the table with numbers of his choice*/
			}
		}
	}
   solve(lines,columns,arr);                     /*function "solve" proceeds*/   
   for (i=0; i<=(lines-1); i++){
      free(arr[i]);
   } 
   free(arr);                   
   return 0;                   
}

void solve(int lines,int columns,int** arr){
    #ifdef PUREREC    /*definition of PUREREC,MATRREC or ITER decides which function will be useed*/ 
    	Purerec(lines,columns,arr);
    #endif
    #ifdef MATRREC
		Matrrec(lines,columns,arr);
    #endif
    #ifdef ITER
		Iter(lines,columns,arr);
    #endif
}
    
void Purerec(int lines,int columns,int** arr)
{
	int i,j,sum,m=0;
    printf("Running pureRecursive");                
    i=-1;
     for (j=0; j<=(columns-1); j++){               /*each possible path starts from the first row of each column*/
    	sum=0;                                    /* to avoid any mistaken output*/
	   	sum=pureRecursive(i,j,lines,columns,arr); /*function call*/  
	  	if (m<sum) m=sum;                         /*saves the maximum sum*/
    }
    printf("\nMax sum is: %d\n",m);
}

void Matrrec(int lines,int columns,int** arr)
{
	int **p,k,i,j,m=0;
	printf("Running matrRecursive");
    p=malloc(lines*sizeof(int*));                 /*creation of a second table p*/
	for (i=0; i<=(lines-1); i++){
		p[i]=malloc(columns*sizeof(int));
    }
    for (i=0; i<=(lines-1); i++){                 
	    for (j=0; j<=(columns-1); j++){
		    if (i==(lines-1)) p[i][j]=arr[i][j];  /*the last line of table p remains the same as the entered table*/
		    if (i<(lines-1)) p[i][j]=-1;          /*every other cell takes the value -1*/
	    }                  /*with the above we avoid the confusion of a sum being 0 or not having calculated any sum yet*/
   	}
	i=-1;
	for (j=0; j<=(columns-1); j++){                     /*for each possible starting path*/
        m=matrRecursive(i,j,lines,columns,arr,p);}     /*function call*/
    for (i=0; i<=(lines-1); i++){                       /*funtion edits table p*/
        m=0;                                           
        if(i==0){
            for (j=0; j<=(columns-1); j++){            /*each cell of table p contains maximum sum*/
		    if (m<p[i][j]){ m=p[i][j]; k=j;}           /*we find the maximum value(sum) for the first line*/
	        }
        printf("\nMax sum is: %d\n%d",m,arr[i][k]);}   
        if (i){
		    if (k==0){
			    m=max(p[i][columns-1],p[i][k],p[i][k+1]);/*the path that can be followed is restricted*/
			    if (m==p[i][columns-1]) k=columns-1;     /*we find the maximum value between 3 cells under the last max found*/ 
			    if (m==p[i][k+1]) k+=1;                  /*var k is used to direct the search of max-value in every repeat*/
			    printf("->%d",arr[i][k]);                /*the i,j-cordinates are used to print the right path*/
			    continue;}
		    if (k==(columns-1)){
			    m=max(p[i][k-1],p[i][k],p[i][0]);
			    if (m==p[i][k-1]) k-=1;
			    if (m==p[i][0]) k=0;
			    printf("->%d",arr[i][k]);
			    continue;}
		    if (k>0 && k<(columns-1)){
			    m=max(p[i][k-1],p[i][k],p[i][k+1]);
			    if (m==p[i][k-1]) k-=1;
			    if (m==p[i][k+1]) k+=1;
			    printf("->%d",arr[i][k]);
			    continue;}
		}
    }printf("\n");
    for (i=0; i<=(lines-1); i++){
        free(p[i]);
    }
    free(p);
}

void Iter(int lines,int columns,int** arr)
{
	int i;
    printf("Running interative");
    interative(arr,lines,columns);  /*function call*/
}
        
